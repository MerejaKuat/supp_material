# -*- coding: utf-8 -*
"""

@author:
"""

import numpy as np
import keras.backend as K
from keras.models import Model
from keras.layers import Dense, Embedding, Lambda, Input, dot, LSTM, Bidirectional, concatenate, TimeDistributed
from interfaces import ModelProvider
from custom_accuracy_metrics import new_sparse_categorical_accuracy as acc
class PosModel(ModelProvider):
    '''
    The model applies Trained W2V embedding with character based biLSTM to extract
    features required for POS task. 2 biLSTM are used to model the POS layer
    followed by a single biLSTM layer to model the POS tags dependency and finally
    a softmax layer is added to predict the POS tag for target input word.
    '''
    def __init__(self, inputWordEmbeddingDimension, maxSentenceLength, maxWordSize, inputWordDictionarySize,
                 inputEmbeddingWeightsFilePath, inputCharDictionarySize, 
                 inputCharEmbeddingDimension, charLSTMUnits, posLSTMUnits,
                 posSequenceLSTMUnits, posCategories):
        # We shall use the keras functional api to build the model
        
        # The input variables
        #   - wordToken is a vocabulary entry (lower-case) representing the input word
        #   - wordSpelling the sequence of characters representing the spelling of the input word
        wordToken = Input(shape=(maxSentenceLength,), name="wordToken")
        wordSpelling = Input(shape=(maxSentenceLength, maxWordSize), name="wordSpelling" )
        
                
        # The pretrained input word embedding 
        inputEmbedding = Embedding(inputWordDictionarySize+1,
                                   inputWordEmbeddingDimension,
                                   mask_zero=True,
                                   input_length=maxSentenceLength,
                                   name='InputEmbedding')
        
        # the input word embedding
        wordEmbeddedVector = inputEmbedding(wordToken)
        
        # trainable input char embedding
        charEmbeddedVector = TimeDistributed(Embedding(inputCharDictionarySize, 
                                                       inputCharEmbeddingDimension,
                                                       input_length=maxWordSize,
                                                       mask_zero=True),
                                                       name='InputCharEmbedding')(wordSpelling)
        # creating character based biLSTM layer
        charLSTM = LSTM(charLSTMUnits)        
        charBiLSTM = Bidirectional(charLSTM) 
        
        wordPosFeatures = TimeDistributed(charBiLSTM, name="char-basedFeaturesLSTM")(charEmbeddedVector)
        
        
        #extract the input to the first POS
        posInput = concatenate([wordEmbeddedVector, wordPosFeatures], name="POS-input")
        
        #creating POS predicting biLSTM
        posPredicter = Bidirectional(
                                    LSTM(posLSTMUnits, return_sequences=True),
                                    name="POSTags-PredicterLSTM")(posInput)
        #create dense to obtain the predicted pos tags
        posTags = TimeDistributed(Dense(posCategories, activation="tanh"), name="PosTagsExtractor")(posPredicter)        
      
        #create POS tags sequence rules detector biLSTM
        posSequenceDetecter = Bidirectional(LSTM(posSequenceLSTMUnits, return_sequences=True),
                                            name="POSTags-SequenceDetecterLSTM")(posTags)
        #creating POS tags output categories layer        
        output = TimeDistributed(Dense(posCategories, activation="softmax"), name="Output")(posSequenceDetecter)

        model = Model(inputs=[wordToken, wordSpelling], output=output, name="POS")
        # loading input embedding weights
        weights = np.load(inputEmbeddingWeightsFilePath)
        weights = np.asarray([weights])
        inputEmbedding.set_weights(weights)
        inputEmbedding.trainable = False
        
        # Compile with the 'sparse_categorical_crossentropy' loss and 'adam' optimizer
        model.compile(optimizer="adam", loss="sparse_categorical_crossentropy", metrics=[acc])
        

        super().__init__(model)
