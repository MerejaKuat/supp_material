# -*- coding: utf-8 -*-
"""

@author: 
"""

import keras.backend as K
from keras.models import Sequential
from keras.layers import Dense, Embedding, Lambda
from interfaces import ModelProvider

class CbowEmbedding(ModelProvider):
    def __init__(self, dictionarySize, embeddingDimension, window=2, padding = True):
        
        model = Sequential()
        
        # Embedding input shape = (batchSize, 2*window) 
        # and output shape = (batchSize, 2*window, embeddingDimension)
        # where in axis=1 we have the context words
        model.add(Embedding(dictionarySize+1,embeddingDimension,
                            mask_zero=padding, input_length=2*window))
        
        # The lambda layer is used to reduce the context words to their mean value
        # producing the output tensor of shape (batchSize, embeddingDimension)
        model.add(Lambda(lambda x: K.mean(x, axis=1)))
        
        # The output layer shall predict the target word selecting it from the 
        # total size of the dictionary
        model.add(Dense(dictionarySize+1, activation='softmax'))
        
        # Compiling the CBOW word2vector embedding model
        model.compile(loss='categorical_crossentropy', optimizer = 'rmsprop', metrics = ['acc'])
        
        super().__init__(model)

