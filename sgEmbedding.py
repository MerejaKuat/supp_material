# -*- coding: utf-8 -*-
"""

@author:
"""
import constants
import numpy as np
import keras.backend as K
from keras.models import  Sequential
from keras.layers import Dense, Embedding, Lambda, Input, dot

def loadEmbeding(filePath, dictionarySize, vectorDim, padding):
    model = Sequential(name = "embeddingProduction")
    embedding = Embedding(dictionarySize,vectorDim, mask_zero=padding, input_length=1, name="pretrained")
    model.add(embedding)
    print(model.summary())
    weights = np.load(filePath)
    weights = np.asarray([weights])
    embedding.set_weights(weights)
    embedding.trainable = False
    target = np.random.randint(1,dictionarySize+1)
    target = np.array([target], np.int32)
    targetRepresentation = model.predict(target)
    print(targetRepresentation)
    

def main():
    loadEmbeding("CBOW-W2V-Similarity-embedding-weights.npy",11388, constants.VECTOR_DIMENSION, True)
if __name__ == '__main__':
    main()
