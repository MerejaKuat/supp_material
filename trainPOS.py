# -*- coding: utf-8 -*-
"""
@author:
"""

#POS Training on Treebank Dataset
import constants
from text_sequence_input import TextSequenceInput as Input
from posModel import PosModel
from posInput import TreebankInput, PosInputPresenterFactory
from training import Trainer


def main():
    # the  POS model shall only be trained on the NTLK Corpora 
    path = None
    corpus = TreebankInput(path).getCorpus()
    #input = Input(corpus, False, (.7,.1,.2), WordEmbeddingPresenterFactory)
    input = Input(corpus, (.7,.1,.2), PosInputPresenterFactory, isLabeled=True, includeCharacterEncoding=True)
    input.initializeWithSavedWordEmbeddingDictionary("Dataset/cbow_data_set")
    wordDictionarySize = input.getWordDictionarySize() 
    charDictionarySize = input.getCharacterDictionarySize()
    posCategories = input.getLabelDictionarySize() 
    
    maxSentenceLength = input.getMaxSequenceLength()
    maxWordLength = input.getMaxWordLength()
    
    wordEmbeddingWeightsPath = "TrainedModels/CBOWWeights.npy"
    wordEmbeddingDimension = constants.VECTOR_DIMENSION
    characterEmbeddingDimension = constants.CHARACTER_VECTOR_DIMESION
    
    posFeaturesLstmUnits = constants.POS_FEATURE_LSTM_UNITS
    posTagsPredictingLstmUnits = constants.POS_TAGS_DEPENDENCY_LSTM_UNITS
    posTagsDependencyLstmUnits = constants.POS_TAGS_DEPENDENCY_LSTM_UNITS
    

    print("Word Dictionary: \t\t\t{}".format(wordDictionarySize))
    print("Character Dictionary: \t\t\t{}".format(charDictionarySize))
    print("POS Tags: \t\t\t\t{}".format(posCategories))
    print("Longest Sentence: \t\t\t{}".format(maxSentenceLength))
    print("Longest Word: \t\t\t\t{}".format(maxWordLength))
    print("Word Embedded Vectors Dimension: \t{}".format(wordEmbeddingDimension))
    print("Character Embedded Vectors Dimension: \t{}".format(characterEmbeddingDimension))
    print("POS Features LSTM Units: \t\t{}".format(posFeaturesLstmUnits))
    print("POS Tags Predicting LSTM Units: \t{}".format(posTagsPredictingLstmUnits))
    print("POS Tags Dependency LSTM Units: \t{}".format(posTagsDependencyLstmUnits))
    
    trainingPresenter = input.getTrainingData()
    validationPresenter = input.getValidationData()
    testPresenter = input.getTestData()
    epochs = 150
    
    
    model = PosModel(wordEmbeddingDimension, maxSentenceLength, maxWordLength,
                     wordDictionarySize, wordEmbeddingWeightsPath, charDictionarySize,
                     characterEmbeddingDimension, posFeaturesLstmUnits,
                     posTagsPredictingLstmUnits, posTagsDependencyLstmUnits, posCategories).getModel()

    training = Trainer(trainingPresenter, validationPresenter, testPresenter, 
                       model, epochs, statPath="TrainingStat", accName = "new_sparse_categorical_accuracy")   
    training.train()
    # serialize model to JSON
    model_json = model.to_json()
    with open("TrainedModels/pos_model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("TrainedModels/pos_model.h5")
    
    # save also the partitioned dataset to file so that evaluation is done on the saved models
    input.save("Dataset/pos_data_set")
    
if __name__ == '__main__':
    main()
    
