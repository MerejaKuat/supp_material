# -*- coding: utf-8 -*-
"""

@author:
"""
import numpy as np
import constants
from text_sequence_input import TextSequenceInput as Input, CbowInputPresenter, WordEmbeddingPresenterFactory, RowTextFile
from cbowSEmbedding import CbowSEmbedding as CbowSEmbedding
from posInput import TreebankInput, PosInputPresenter
from training import Trainer


def main():
    # NLTK Corpus Path  
    path = r"D:\\Personal\\Emmajack\\Projects\\Corpus\\nltk_data"
    corpus = TreebankInput(path).getCorpus()
    
    # Input System with corpus and (Training=70%, Validation = 10% and Test = 20%) split ratio
    input = Input(corpus, (.7,.1,.2), WordEmbeddingPresenterFactory, isLabeled=True, includeCharacterEncoding=False)
    
    dictionarySize = input.getWordDictionarySize()
    print("Dictionary: {}".format(dictionarySize))
    
    # Using CboInputPresenter as a decorator (Decorator design pattern) to the WordEmbeddingPresenter
    # Produces 25% Negative Samples
    trainingPresenter = CbowInputPresenter(input.getTrainingData(), constants.BATCH_SIZE, 
                                           constants.W2V_WINDOW_SIZE, dictionarySize, negativeSamplesRate = 0.25)
    validationPresenter = CbowInputPresenter(input.getValidationData(), constants.BATCH_SIZE, 
                                             constants.W2V_WINDOW_SIZE, dictionarySize, negativeSamplesRate = 0.25)
    testPresenter = CbowInputPresenter(input.getTestData(), constants.BATCH_SIZE, 
                                       constants.W2V_WINDOW_SIZE, dictionarySize, negativeSamplesRate = 0.25)
    
    epochs = 100
    model = CbowSEmbedding(dictionarySize, constants.VECTOR_DIMENSION, window=constants.W2V_WINDOW_SIZE).getModel()
    training = Trainer(trainingPresenter, validationPresenter, testPresenter, model, epochs)   
    training.train()
    
    # Saving the trained embedding weight matrix to pos-embedding-weights.npy file
    embedding = model.get_layer(name="embedding")
    weightsAndBiases = embedding.get_weights()
    np.save("pos-embedding-weights", weightsAndBiases[0])
   
    
if __name__ == '__main__':
    main()
